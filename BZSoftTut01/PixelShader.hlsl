struct VS_OUTPUT
{
	float4 pos : SV_POSITION;
	float4 color : COLOR;
};

//PS必须返回一个由SV_TARGET语义关联的float4数据，SV_TARGET是系统值语义，被管线使用
//VS中的属性（例如这个color）将会在三角形表面进行内插（光栅化阶段），对于三角形上的每个像素，这个内插的值被传递到PS中进行处理
float4 main(VS_OUTPUT input) : SV_TARGET
{
	// 返回插值的颜色
	return input.color;
}